""" dlgis package configuration
"""
from typing import Any, Dict
import setuptools  # type: ignore

about: Dict[Any, Any] = {}
with open("src/dlgis/__about__.py") as f:
    exec(f.read(), about)

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name='dlgis',
    version=about["version"],
    author="IRI, Columbia University",
    author_email="help@iri.columbia.edu",
    description="Data Library GIS datasets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/iridl/dlgis",
    package_dir={'': 'src'},
    packages=['dlgis'],
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">= 3.7",
    install_requires=[
        "click == 8.0.*",
        "pyshp == 2.1.*",
        "gdal == 3.3.*",  # conda package
        # requires shp2pgsql 2.5.*, psql, grep and sed  binaries
    ],
    entry_points = {
        'console_scripts': [
            # Warning: these must be specified redundantly in the
            # conda recipe. Otherwise they work on linux and macos but
            # not on windows.
            'dlgis_import=dlgis:import_shapes',
            'dlgis_export=dlgis:export_shapes',
        ],
    },
    project_urls={"Bug Reports": "https://bitbucket.org/iridl/dlgis/issues"},
)
